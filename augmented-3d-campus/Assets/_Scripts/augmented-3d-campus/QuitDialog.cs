﻿using UnityEngine;
using System.Collections;

public class QuitDialog : MonoBehaviour {

	void Start() {
		Hidden = true;
	}

	private bool hidden;
	/// <summary>
	/// Gets or sets a value indicating whether <see cref="QuitDialog"/> is hidden.
	/// </summary>
	/// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
	public bool Hidden {
		get {
			return hidden;
		}
		set {
			hidden = value;
			this.gameObject.GetComponent<Canvas> ().enabled = !hidden;
		}
	}

	/// <summary>
	/// Quits app.
	/// </summary>
	public void Quit() {
		Debug.Log ("Application.Quit ()");
		Application.Quit ();
		Hidden = true;
	}

	/// <summary>
	/// Hides this <see cref="QuitDialog"/>
	/// </summary>
	public void Cancel() {
		Hidden = true;
	}
}
