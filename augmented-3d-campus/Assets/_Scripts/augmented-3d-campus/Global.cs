﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

/// <summary>
/// Global helper methods.
/// </summary>
public class Global {
	
	/// <summary>
	/// Holds all buildings.
	/// </summary>
	public static Dictionary<string, Building> BuildingList = new Dictionary<string, Building>();
	
	/// <summary>
	/// Deselects all buildings.
	/// </summary>
	public static void DeselectAllBuildings()
	{
		foreach(var building in BuildingList) {
			building.Value.Selected = false;
		}
	}

	/// <summary>
	/// Gets the campus data.
	/// </summary>
	/// <returns>The campus data.</returns>
	public static CampusData GetCampusData() {
		return GameObject.Find ("Global").GetComponent<CampusData> ();
	}

	/// <summary>
	/// Gets the sidebar.
	/// </summary>
	/// <returns>The sidebar.</returns>
	public static Sidebar GetSidebar() {
		return GameObject.Find ("Sidebar").GetComponent<Sidebar> ();
	}

	/// <summary>
	/// Gets the touch visualizer.
	/// </summary>
	/// <returns>The touch visualizer.</returns>
	public static TouchVisualizer GetTouchVisualizer() {
		return GameObject.Find ("Touch Visualizer").GetComponent<TouchVisualizer> ();
	}

	/// <summary>
	/// Gets the quit dialog.
	/// </summary>
	/// <returns>The quit dialog.</returns>
	public static QuitDialog GetQuitDialog() {
		return GameObject.Find ("Quit Dialog").GetComponent<QuitDialog> ();
	}

	/// <summary>
	/// Gets the quit dialog.
	/// </summary>
	/// <returns>The quit dialog.</returns>
	public static InfoDialog GetInfoDialog() {
		return GameObject.Find ("Info Dialog").GetComponent<InfoDialog> ();
	}


	/// <summary>
	/// Sets the UI direction
	/// </summary>
	/// <returns>The quit dialog.</returns>
	public static void SetUIDirection(bool right) {
		GetTouchVisualizer ().Right = right;
		GetSidebar ().Right = right;
	}

	/// <summary>
	/// Gets the camera.
	/// </summary>
	/// <returns>The AR camera.</returns>
	public static Camera getCamera () {
		var cam = GameObject.Find ("Camera");
		if (cam != null) {
			return cam.GetComponent<Camera> ();
		} else {
			cam = GameObject.Find ("StereoCameraLeft");
			if (cam != null) {
				return cam.GetComponent<Camera> ();
			} 
		}
		return null;
	}

	/// <summary>
	/// Gets the search window.
	/// </summary>
	/// <returns>The search window.</returns>
	public static SearchWindow GetSearchWindow() {
		return GameObject.Find ("Search Window").GetComponent<SearchWindow>();
	}

	/// <summary>
	/// Hides the panels.
	/// </summary>
	public static void HidePanels() {
		Global.GetQuitDialog ().Hidden = true;
		Global.GetSidebar ().Hidden = true;
		Global.GetTouchVisualizer ().Hidden = true;
		Global.GetSearchWindow ().Hidden = true;
		Global.GetInfoDialog ().Hidden = true;
	}

	/// <summary>
	/// Starts the pong.
	/// </summary>
	public static void StartPong()
	{
		SceneManager.LoadScene("Pong");
	} 

	/// <summary>
	/// Stops the pong.
	/// </summary>
	public static void StopPong()
	{
		BuildingList.Clear ();
		SceneManager.LoadScene("Augmented 3D Campus");
	}
}
