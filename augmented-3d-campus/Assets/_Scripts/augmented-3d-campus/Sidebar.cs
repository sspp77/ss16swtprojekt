﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Sidebar : MonoBehaviour {
	[Header("Behaviour")]

	/// <summary>
	/// The animation speed.
	/// </summary>
	public float AnimationSpeed;

	/// <summary>
	/// Is the sidebar hidden.
	/// </summary>
	private bool hidden = true;
	public bool Hidden{
		get {
			return hidden;
		}
		set {
			if (!value) {
				Global.HidePanels ();
			}
			hidden = value;
		}
	}

	/// <summary>
	/// Defines the location
	/// </summary>
	public bool Right;

	[Header("UI Text Elements")]

	/// <summary>
	/// The title label. Default is "Gebäude".
	/// </summary>
	public Text TitleText;

	/// <summary>
	/// The text which holds the building title.
	/// </summary>
	public Text BuildingTitleText;

	/// <summary>
	/// The text which holds buildings contents.
	/// </summary>
	public Text ContentsText;

	/// <summary>
	/// The text which holds facultiy names.
	/// </summary>
	public Text FacultiesText;

	/// <summary>
	/// The link to the rect transform component
	/// </summary>
	public RectTransform RectTransform;

	/// <summary>
	/// The link to the parent canvas
	/// </summary>
	public Canvas ParentCanvas;

	/// <summary>
	/// The left close button
	/// </summary>
	public GameObject LeftCloseButton;

	/// <summary>
	/// The right close button
	/// </summary>
	public GameObject RightCloseButton;

	/// <summary>
	/// Updates the hide and show animations
	/// </summary>
	void Update () {

		if (Right) {
			RightCloseButton.SetActive (false);
			LeftCloseButton.SetActive (true);
		} else {
			RightCloseButton.SetActive (true);
			LeftCloseButton.SetActive (false);
		}

		RectTransform = GetComponent<RectTransform> ();

		var screenWidth = Screen.width / ParentCanvas.GetComponent<RectTransform>().localScale.x;
		var width = RectTransform.rect.width;

		if (Right) {
			/* position switch */
			if (RectTransform.anchoredPosition.x < 0) {
				RectTransform.anchoredPosition = new Vector2 (width, 0);
			}

			/* hide animation */
			if (Hidden && RectTransform.anchoredPosition.x <= width) {
				RectTransform.anchoredPosition = new Vector2 (RectTransform.anchoredPosition.x + AnimationSpeed * Time.deltaTime, 0);
			}
			if (Hidden && RectTransform.anchoredPosition.x > width) {
				RectTransform.anchoredPosition = new Vector2 (width, 0);
			}

			/* show animation */
			if (!Hidden && RectTransform.anchoredPosition.x >= 0) {
				RectTransform.anchoredPosition = new Vector2 (RectTransform.anchoredPosition.x - AnimationSpeed * Time.deltaTime, 0);
			}
			if (!Hidden && RectTransform.anchoredPosition.x < 0) {
				RectTransform.anchoredPosition = new Vector2 (0, 0);
			}

		} else {/* position switch */
			if (RectTransform.anchoredPosition.x > -screenWidth + width) {
				RectTransform.anchoredPosition = new Vector2 (-screenWidth, 0);
			}

			/* hide animation */
			if (Hidden && RectTransform.anchoredPosition.x >= -screenWidth) {
				RectTransform.anchoredPosition = new Vector2 (RectTransform.anchoredPosition.x - AnimationSpeed * Time.deltaTime, 0);
			}
			if (Hidden && RectTransform.anchoredPosition.x < -screenWidth) {
				RectTransform.anchoredPosition = new Vector2 (-screenWidth, 0);
			}

			/* show animation */
			if (!Hidden && RectTransform.anchoredPosition.x <= -screenWidth + width) {
				RectTransform.anchoredPosition = new Vector2 (RectTransform.anchoredPosition.x + AnimationSpeed * Time.deltaTime, 0);
			}
			if (!Hidden && RectTransform.anchoredPosition.x > -screenWidth + width) {
				RectTransform.anchoredPosition = new Vector2 (-screenWidth + width, 0);
			}
		}

	}

	/// <summary>
	/// Hides the sidebar and deselects all buildings.
	/// </summary>
	public void Hide() 
	{
		Global.DeselectAllBuildings ();
		Hidden = true;
		GameObject.Find ("Touch Visualizer").GetComponent<TouchVisualizer> ().Hidden = true;
	}

	private string buildingIdentifier;
	/// <summary>
	/// Sets building identifier and loads the CampusData.
	/// </summary>
	public string BuildingIdentifier
	{
		set {
			buildingIdentifier = value;
			CampusData campusData = Global.GetCampusData();
			BuildingData buildingData = campusData.Buildings [buildingIdentifier];
			
			// Set Building Title
			BuildingTitleText.text = "Gebäude " + buildingIdentifier;

			// Set Building Contents
			var contents = buildingData.Contents;
			if (contents.Count.Equals (0)) {
				ContentsText.text = "-";
			} else {
				string contentsString = "";
				foreach (string content in contents) {
					contentsString += content + "\n";
				}
				contentsString = contentsString.TrimEnd ('\n');
				ContentsText.text = contentsString;
			}


			// Set Building Faculties
			var faculties = buildingData.Faculties;
			if (faculties.Count.Equals (0)) {
				FacultiesText.text = "-";
			} else {
				string facultiesString = "";
				foreach (string facultiy in faculties) {
					string facultiyShort = campusData.Faculties [facultiy].Short;
					string facultiyLong = campusData.Faculties [facultiy].Long;
					facultiesString += facultiyLong + " (" + facultiyShort + ")\n";
				}
				facultiesString = facultiesString.TrimEnd ('\n');
				FacultiesText.text = facultiesString;
			}
		}
		get {
			return buildingIdentifier;
		}
	}

	/// <summary>
	/// Opens google maps.
	/// </summary>
	public void openGoogleMaps() {
		// Load global campus data
		CampusData campusData = Global.GetCampusData();

		// Find building with identifier
		BuildingData buildingData = campusData.Buildings [buildingIdentifier];

		// Read longitude and latitude
		var longitude = buildingData.Longitude;
		var latitude = buildingData.Latitude;

		/* Build URL
		 * 
		 * Google Maps web dervie paramater:
		 * z: The zoom level (1-20)
		 * t: The map type ("m" map, "k" satellite, "h" hybrid, "p" terrain, "e" GoogleEarth)
		 * q: The search query, if it is prefixed by loc: then google assumes it is a lat lon separated by a +
		 */
		var z = 20;
		var t = "m";
		var q = "loc:" + WWW.EscapeURL (latitude) + "+" + WWW.EscapeURL (longitude);
		var url = "http://maps.google.com/maps?z=" + z + "&t=" + t + "&q=" + q;

		Debug.Log ("will open " + buildingIdentifier + " in google maps: " + url);
		Application.OpenURL(url);
	}

	/// <summary>
	/// Opens the THM site.
	/// </summary>
	public void openTHMSite() {
		var url = "http://www.thm.de/site/hochschule/campus/campus-giessen/online-campusplan-giessen.html";
		Application.OpenURL(url);
	}

    /// <summary>
	/// Opens the THM INFORMATION site.
	/// </summary>
    public void openINFOSite() {
        var url = "http://www.thm.de/site/hochschule/campus/information.html";
        Application.OpenURL(url);
    }
}
