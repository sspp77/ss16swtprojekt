﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public float velocity = 2f;

    public Transform ball;

    private Rigidbody rb;

	// Use this for initialization
	void Start () { 
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(ball.position.x >= transform.position.x)
        {
            rb.velocity = new Vector3(1, 0, 0) * Mathf.Min(velocity,Mathf.Abs(ball.position.x - transform.position.x) *10);
        } else
        {
            rb.velocity = new Vector3(-1, 0, 0) * Mathf.Min(velocity, Mathf.Abs(ball.position.x - transform.position.x)*10);
        }
	}
}
