﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    public int scorePlayer;
    public int scoreComputer;

    public TextMesh enemyScore;
    public TextMesh playerScore;

    public bool started = false;

	
	// Update is called once per frame
	void Update () {
		if (!started && Input.GetMouseButtonDown(0)) started = true;
        enemyScore.text = scoreComputer.ToString();
        playerScore.text = scorePlayer.ToString();
	}

	public void StopPong() {
		Global.StopPong ();
	}
}
