﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PlayerController : MonoBehaviour {

    public static String BatIdentifier = "A20";
    public Slider Slider;

	// Use this for initialization
	void Start () {
		setBat();
	}

    void Update()
    {
        transform.position = new Vector3(Slider.value * 16, transform.position.y, transform.position.z);
    }

    void setBat()
    {
		if (BatIdentifier.Equals ("")) {
			Debug.Log ("No Bat Identifier");
			return;
		}

		GameObject bat = Instantiate(Resources.Load<GameObject>("augmented-3d-campus/PongBats/" + BatIdentifier)) as GameObject;
        bat.transform.localPosition = transform.localPosition;
        bat.transform.SetParent(transform);

        GameObject.FindGameObjectWithTag("Ball").GetComponent<BallController>().resetBall();
    }
}
