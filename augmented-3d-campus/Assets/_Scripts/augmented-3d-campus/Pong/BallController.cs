﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

    public float velocity = 4.0f;
    public Vector3 direction; 

    public float velocityModifier = 2.0f;

    public GameController gameController;
    public GameObject ballPrefab;

    private Rigidbody rb;

    private bool isDead = false;

    private Vector3 velo;
     
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        direction = new Vector3(Random.Range(2, 5),0, Random.Range(4, 6)).normalized;
    }
	
	// Update is called once per frame
	void Update () { 
        if(gameController.started) rb.velocity = direction.normalized * velocity;
        velo = rb.velocity;
    }

    void OnCollisionEnter(Collision col)
    {
        if(!isDead)
        {
            switch (col.gameObject.tag)
            {
                case "SideLeft":
                    direction = Vector3.Reflect(velo, col.contacts[0].normal);//Mathf.Abs(direction.x);
                    break;
                case "SideRight":
                    direction = Vector3.Reflect(velo, col.contacts[0].normal);//-Mathf.Abs(direction.x);
                    break;
                case "Enemy":
                    direction = Vector3.Reflect(velo, col.contacts[0].normal);//new Vector3(direction.x, 0, -Random.Range(.4f, 1)).normalized;
                    velocity += velocityModifier;
                    break;
                case "Player":
                    for(int i = 0; i < col.contacts.Length; i++) direction = Vector3.Reflect(velo, col.contacts[i].normal);//new Vector3(direction.x,0,Random.Range(.4f,1)).normalized;
                    velocity += velocityModifier;
                    break;
                case "EnemyField":
                    gameController.scorePlayer++;
                    resetBall();
                    break;
                case "PlayerField":
                    gameController.scoreComputer++;
                    resetBall();
                    break;
            }
            
        }
        Debug.Log(col.gameObject.tag);
    }

    public void resetBall()
    {
        GameObject newBall = Instantiate(ballPrefab) as GameObject;
        GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyController>().ball = newBall.transform;
        newBall.GetComponent<BallController>().ballPrefab = this.ballPrefab;
        newBall.GetComponent<BallController>().gameController = this.gameController;
        isDead = true;
        gameController.started = false;
        Destroy(this.gameObject);
    }
}
