﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class InputHandler : MonoBehaviour {

	/// <summary>
	/// Handles key events globally
	/// </summary>
	void Update () {
		handleKeys ();

		if (Input.GetMouseButtonDown(0)) {
			handleTouch ();
		}
	}

	private void handleTouch() {
		// Check if finger is over a UI element


		if (Input.touches.Length == 1) {
			/* touch */
			if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId)) {
				raycast ();
			}
		} else {
			/* mouse */
			if (!EventSystem.current.IsPointerOverGameObject ()) {
				raycast ();
			}
		}
	}

	private void raycast() {
		RaycastHit hit;
		var camera = Global.getCamera ();
		if (camera == null) {
			return;
		}
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);

		if (Physics.Raycast (ray, out hit)) {

			Debug.Log ("hit: " + hit.collider.gameObject.name);
				
			var gameObject = GameObject.Find (hit.collider.gameObject.name);
			if (gameObject == null) {
				return;
			}
			if (gameObject.tag == "Building") {
				var building = gameObject.GetComponent<Building> ();

				Global.DeselectAllBuildings ();
				building.Selected = true;

				var touchVisualizer = Global.GetTouchVisualizer ();
				touchVisualizer.Hidden = false;

				touchVisualizer.Position = hit.point;
			} else if (gameObject.name == "Background") {
				Global.GetSidebar ().Hide ();
			}

		} else {
			Global.GetSidebar ().Hide ();
		}
	}

	private void handleKeys() {
		handleESC ();
	}

	private void handleESC() {
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{ 
			Debug.Log ("Escape / Back Key event!");
			if (!Global.GetTouchVisualizer ().Hidden) 
			{
				Global.GetSidebar ().Hide ();
				return;
			} 

			if (!Global.GetQuitDialog ().Hidden) {
				Global.GetQuitDialog ().Hidden = true;
				return;
			}

			if (!Global.GetInfoDialog ().Hidden) {
				Global.GetInfoDialog ().Hidden = true;
				return;
			}

			if (!Global.GetSearchWindow ().Hidden) {
				Global.GetSearchWindow ().Hidden = true;
				return;
			}


			Global.GetQuitDialog ().Hidden = false;
		}
	}
}
