﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class SpawnStudents : MonoBehaviour, ITrackableEventHandler
{

    private TrackableBehaviour mTrackableBehaviour;

    public float spawnIntervalMin = 1.0f;
    public float spawnIntervalMax = 3.0f;

    public GameObject studentPrefab;

    public GameObject tracker;

    public StudentBase.WayPoint[] waypoints;

    public float studentSpeed = 5.0f;

    public bool loop = true;
    public bool destroyWhenDone = false;

    public float radius = 2.0f;

    private float timePassed = 0.0f;
    private float nextSpawnInterval;

    private bool tracked = false;
    

	// Use this for initialization
	void Start () {
        nextSpawnInterval = Random.Range(spawnIntervalMin, spawnIntervalMax);


        if (tracker == null)
        {
            tracked = true;
            return;
        }

        mTrackableBehaviour = tracker.GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (tracked)
        {

            if (timePassed >= nextSpawnInterval)
            {
                Vector2 randRad = Random.insideUnitCircle * radius;
                Vector3 pos = new Vector3(transform.position.x + randRad.x, transform.position.y, transform.position.z + randRad.y);
                GameObject cur = Instantiate(studentPrefab, pos, Quaternion.identity) as GameObject;

                StudentBase.WayPoint[] newWP = new StudentBase.WayPoint[waypoints.Length];
                for (int i = 0; i < waypoints.Length; i++)
                {
                    newWP[i] = new StudentBase.WayPoint();
                    newWP[i].location = waypoints[i].location;
                    newWP[i].radius = waypoints[i].radius;
                    newWP[i].waitDuration = waypoints[i].waitDuration;
                }

                StudentBase curBase = cur.GetComponent<StudentBase>();

                curBase.tracker = tracker;
                curBase.waypoints = newWP;
                curBase.loop = loop;
                cur.transform.parent = transform;
                curBase.maxSpeed = studentSpeed;
                curBase.destroyWhenDone = destroyWhenDone;
                nextSpawnInterval = Random.Range(spawnIntervalMin, spawnIntervalMax);
                timePassed = 0.0f;
            }
            else timePassed += Time.deltaTime;
        }
	}

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.TRACKED || 
            newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) tracked = true;
        else if (newStatus == TrackableBehaviour.Status.NOT_FOUND) tracked = false;
    }
}
