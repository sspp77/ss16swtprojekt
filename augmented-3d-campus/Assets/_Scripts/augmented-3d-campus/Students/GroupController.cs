﻿using UnityEngine;
using System.Collections;

public class GroupController : StudentBase
{
    protected override void initAnimation()
    {
        anim = GetComponentsInChildren<Animator>();
    }

    protected override void stand()
    {
        foreach (Animator a in anim) a.SetBool("Walking", false);
    }

    protected override void walk(int i = 0)
    {
        if (waitTime[i] <= 0)
        {
            anim[i].SetBool("Walking", true);
        }
        else waitTime[i] -= Time.deltaTime;
    }
}