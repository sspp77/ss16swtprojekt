﻿using UnityEngine;
using System.Collections;
using Vuforia;

public abstract class StudentBase : MonoBehaviour, ITrackableEventHandler
{
    [System.Serializable]
    public class WayPoint
    {
        public Transform location;
        public float waitDuration;
        public float radius;

        private Vector3 actualPos;

        public Vector3 ActPos
        {
            get
            {
                return actualPos;
            }
        }

        public void newPos()
        {
            Vector2 randRad = Random.insideUnitCircle * radius;
            actualPos = new Vector3(location.position.x + randRad.x, location.position.y, location.position.z + randRad.y);
        }
    }

    public WayPoint[] waypoints;
    public float maxSpeed = 5.0f;
    public bool loop = true;
    public bool destroyWhenDone = false;

    public GameObject tracker;

    protected float alreadyWaited = 0;
    protected int currentWayPoint = 0;
    protected Animator[] anim;

    private TrackableBehaviour mTrackableBehaviour;

    protected bool tracked = false;

    protected float[] waitTime;

    // Use this for initialization
    void Start () {
        initAnimation();
        if (waypoints.Length == 0 && destroyWhenDone) Destroy(gameObject);

        waitTime = new float[anim.Length];
        for (int i = 0; i < waitTime.Length; i++) waitTime[i] = Random.Range(0, .5f);
        foreach (WayPoint w in waypoints) w.newPos();

        setTracker(tracker);
    }

    public void setTracker(GameObject t)
    {
        if (tracker == null)
        {
            tracked = true;
            return;
        }

        mTrackableBehaviour = t.GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(tracked)
        {
            if (currentWayPoint < waypoints.Length)
            {
                Vector3 posA = transform.position;
                Vector3 posB = waypoints[currentWayPoint].ActPos;

                if (Vector3.Distance(posA, posB) < 0.1)
                {
                    if (alreadyWaited >= waypoints[currentWayPoint].waitDuration)
                    {
                        waypoints[currentWayPoint].newPos();
                        currentWayPoint += 1;
                        for (int i = 0; i < waitTime.Length; i++) waitTime[i] = Random.Range(0, .5f);
                    }

                    stand();
                    alreadyWaited += Time.deltaTime;

                }
                else
                {
                    for (int i = 0; i < anim.Length; i++)
                    {

                        walk(i);
                        float step = maxSpeed * Time.deltaTime;
                        transform.position = Vector3.MoveTowards(posA, posB, step);
                    }
                }

            }
            else
            {
                if (loop)
                {
                    alreadyWaited = 0;
                    currentWayPoint = 0;
                }
                else if (destroyWhenDone) Destroy(gameObject);
            }
        }
    }

    protected abstract void initAnimation();
    protected abstract void walk(int i = 0);
    protected abstract void stand();

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) tracked = true;
        else if (newStatus == TrackableBehaviour.Status.NOT_FOUND) tracked = false;
    }
}
