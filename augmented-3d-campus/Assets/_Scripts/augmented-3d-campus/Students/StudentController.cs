﻿using UnityEngine;
using System.Collections;

public class StudentController : StudentBase
{
    protected override void initAnimation()
    {
        anim = new Animator[1];
        anim[0] = GetComponent<Animator>();
    }

    protected override void walk(int i = 0)
    {
        anim[i].SetBool("Walking", true);
    }

    protected override void stand()
    {
        anim[0].SetBool("Walking", false);
    }
}