﻿using UnityEngine;
using System.Collections;

public class MarkerBehaviour : MonoBehaviour {
	private int moveDirection = 1;

	/// <summary>
	/// The minimum y position.
	/// </summary>
	public float MinimumY;
	/// <summary>
	/// The maximum y position.
	/// </summary>
	public float MaximumY;

	/// <summary>
	/// The move speed.
	/// </summary>
	public float MoveSpeed;
	/// <summary>
	/// The rotation speed.
	/// </summary>
	public float RotationSpeed;

    void Update() {

		if (transform.localPosition.y > MaximumY) {
			moveDirection = 1; 
		}
		else if(transform.localPosition.y < MinimumY) {
			moveDirection = -1; 
		}

		transform.Translate (Vector3.forward * Time.deltaTime * moveDirection * MoveSpeed);

		transform.Rotate(Vector3.forward * Time.deltaTime * RotationSpeed);

    }
}
