﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// Represents a building GameObject.
/// </summary>
public class Building : MonoBehaviour {

	private Material defaultMat;
	private Material selectionMat;
	private bool selected;
	private Sidebar sidebar;

	private int easterEggPongClickCounter = 0;

	private const int EASTER_EGG_CLICK_TARGET = 5;


	public int IndexOfGreenMaterial;

	void Start () {
		Global.BuildingList.Add (name, this);
		defaultMat = new Material (GetComponentInChildren<Renderer> ().materials [IndexOfGreenMaterial]);
		selectionMat = Resources.Load("augmented-3d-campus/Selection Mat", typeof(Material)) as Material;

		sidebar = Global.GetSidebar ();
	}

	void Update() {

		if (easterEggPongClickCounter == EASTER_EGG_CLICK_TARGET)
        {
			easterEggPongClickCounter = 0;
			PlayerController.BatIdentifier = name;
            Global.StartPong ();

        }
		var boxCollider = GetComponent<BoxCollider> ();
		if (boxCollider != null) {
			if (!boxCollider.enabled) {
                easterEggPongClickCounter = 0;
				Selected = false;
				Global.GetSidebar ().Hide ();
				Global.GetTouchVisualizer ().Hidden = true;
			}
		} else {
			Debug.Log ("No Boxcollider attached to " + name);
		}
	}

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Building"/> is selected.
	/// </summary>
	/// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
	public bool Selected {
		get {
			return selected;
		}
		set {
			selected = value;
			if (gameObject != null) {
				gameObject.GetComponentInChildren<Renderer> ().materials [IndexOfGreenMaterial].CopyPropertiesFromMaterial (selected ? selectionMat : defaultMat);
			}
			if (selected) {
				sidebar.BuildingIdentifier = name;
				sidebar.Hidden = false;
                easterEggPongClickCounter++;
			}
		}
	}
}
