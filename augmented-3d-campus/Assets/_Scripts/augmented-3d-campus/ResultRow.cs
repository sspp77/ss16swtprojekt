﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class ResultRow : MonoBehaviour {

	public Text Name;
	public Text Faculty;
	public Text Match;

	private String buildingKey;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Fill(ArrayList match) {
		if (match[0] != null) {
			buildingKey = (String)match[0];

			Name.text = (String)match[0];
			Match.text = (String)match [1];

			var facultyCount = Global.GetCampusData ().Buildings [(String)match[0]].Faculties.Count;

			var facText = "";
			if (facultyCount > 0) {
				var faculties = Global.GetCampusData ().Buildings [(String)match[0]].Faculties;
				foreach (var faculty in faculties) {
					facText += Global.GetCampusData ().Faculties [faculty].Short + ", ";
				}
				facText = facText.Substring (0, facText.Length - 2);
			} else {
				facText += "-";
			}
			Faculty.text = facText;
		}
	}

	public void ShowBuilding() {
		Global.GetSidebar ().BuildingIdentifier = buildingKey;
		Global.GetSidebar ().Hidden = false;
		Global.GetSearchWindow ().Hidden = true;

		var building = GameObject.Find (buildingKey).GetComponent<Building>();
		building.Selected = true;
		Global.GetTouchVisualizer ().Position = building.gameObject.transform.position;
		Global.GetTouchVisualizer ().Hidden = false;
	}
}
