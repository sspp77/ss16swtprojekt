﻿using UnityEngine;
using System.Collections;

public class FPSOverlay : MonoBehaviour {

	// Use this for initialization
	int index;
	double[] fps;

	private const int BUFFER_SIZE = 20;

	void Start () {
		fps = new double[BUFFER_SIZE];
		index = 0;

		Debug.Log ("Version: " + Vuforia.VuforiaUnity.GetVuforiaLibraryVersion ());
	}
	
	// Update is called once per frame
	void Update () {
		fps [index++] = 1.0 / Time.deltaTime;
		index = index % BUFFER_SIZE;

	}

	double getFPS() {
		double akku = 0;
		for (int i = 0; i < BUFFER_SIZE; i++) {
			akku += fps [i];
		}
		akku = akku / BUFFER_SIZE;
		return akku;
	}

	void OnGUI() {
		GUILayout.Button ("FPS: " + (int)getFPS());
	}
}
