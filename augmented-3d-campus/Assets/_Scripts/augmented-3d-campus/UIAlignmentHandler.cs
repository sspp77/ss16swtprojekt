﻿using UnityEngine;
using System.Collections;

public class UIAlignmentHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var touchVisualizer = Global.GetTouchVisualizer ();


		if (!touchVisualizer.Hidden) {
			if (touchVisualizer.GetTouchVisualizer2DPosition ().x > Screen.width * 0.55f) {
				Global.SetUIDirection (false);
			}
			if (touchVisualizer.GetTouchVisualizer2DPosition().x < Screen.width * 0.45f) {
				Global.SetUIDirection (true);
			}
		}
	}
}
