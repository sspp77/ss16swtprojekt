﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
/// <summary>
/// The TouchVisualizer Controller Class. Handles the position of the TouchVisualizer and its components automatically.
/// It is handled by the Building Class.
/// </summary>
public class TouchVisualizer : MonoBehaviour {
	
	[Header("UI Elements")]
	public GameObject TouchVisualizerSprite;
	public GameObject VerticalLine;
	public GameObject HorizontalLine;
	public RectTransform canvasRect;

	/// <summary>
	/// The Position of the TouchVisualizer in 3D (Aligned to Center)
	/// </summary>
	public Vector3 Position;

	[Header("Behaviour")]
	/// <summary>
	/// Is the TouchVisualizer Hidden
	/// </summary>
	public bool Hidden;

	/// <summary>
	/// Radius of the circle for calculations
	/// </summary>
	public float CircleRadius;

	/// <summary>
	/// Thickness of the lines
	/// </summary>
	public float LineThickness;

	/// <summary>
	/// Defines the alignment
	/// </summary>
	public bool Right;

	void Start() {
		Hidden = true;
	}

	/// <summary>
	/// Returns the 2D position of the visualizer sprite in the screen.
	/// </summary>
	public Vector2 GetTouchVisualizer2DPosition() {
		return TouchVisualizerSprite.GetComponent<RectTransform> ().position;
	}

	void Update () {
		if(Global.getCamera() == null) {
			return ;
		}
		TouchVisualizerSprite.GetComponent<RectTransform> ().position = new Vector2(Global.getCamera().WorldToScreenPoint (Position).x, Global.getCamera().WorldToScreenPoint (Position).y);

		TouchVisualizerSprite.GetComponent<RectTransform> ().localScale = canvasRect.localScale;

		TouchVisualizerSprite.GetComponent<Image> ().enabled = !Hidden;
		VerticalLine.GetComponent<Image> ().enabled = !Hidden;
		HorizontalLine.GetComponent<Image> ().enabled = !Hidden;

		calculateHorizontalLine ();
		calculateVerticalLine ();
	}

	private void calculateHorizontalLine() {
		var PositionX = TouchVisualizerSprite.GetComponent<RectTransform> ().position.x;
			
		var width = 0f;
		if (Right) {
			width = ((Screen.width - PositionX) / canvasRect.localScale.x) + (LineThickness * canvasRect.localScale.x / 2f);
		} else {
			width = (PositionX / canvasRect.localScale.x) + (LineThickness * canvasRect.localScale.x / 2f);
		}
		var height = LineThickness * canvasRect.localScale.y;

		var posX = 0f;
		if (Right) {
			posX = Screen.width - (width * canvasRect.localScale.x / 2f);
		} else {
			posX = (width * canvasRect.localScale.x / 2f);
		}
		var posY = Screen.height / 2f;

		HorizontalLine.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, height);
		HorizontalLine.GetComponent<RectTransform> ().position = new Vector2 (posX, posY);
	}

	private void calculateVerticalLine() {
		var PositionX = TouchVisualizerSprite.GetComponent<RectTransform> ().position.x;
		var PositionY = TouchVisualizerSprite.GetComponent<RectTransform> ().position.y;

		var width = LineThickness * canvasRect.localScale.x;
		var height = ((Mathf.Abs(Screen.height / 2f - PositionY) / canvasRect.localScale.y) - (CircleRadius * canvasRect.localScale.x));

		var posX = PositionX;
		var posY =  (Screen.height / 2f) - (height * canvasRect.localScale.y) / 2f;

		if (PositionY > Screen.height / 2f) {
			posY = (Screen.height / 2f) + (height * canvasRect.localScale.y) / 2f;
		}

		VerticalLine.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, height);
		VerticalLine.GetComponent<RectTransform> ().position = new Vector2 (posX, posY);
	}
}
