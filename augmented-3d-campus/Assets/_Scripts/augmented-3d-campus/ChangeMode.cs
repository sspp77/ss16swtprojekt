﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System.Collections.Generic;

public class ChangeMode : MonoBehaviour {

    private DigitalEyewearBehaviour eyewear;

    // Use this for initialization
    void Start () {
        eyewear = GetComponent<DigitalEyewearBehaviour>();
    }

    // Update is called once per frame
    public void change()
	{
		Global.GetSidebar ().Hide ();
		Global.GetTouchVisualizer ().Hidden = true;

        if (eyewear.GetEyewearType() == DigitalEyewearAbstractBehaviour.EyewearType.None)
        {
            DigitalEyewearBehaviour.Instance.SetEyewearType(DigitalEyewearAbstractBehaviour.EyewearType.VideoSeeThrough);
            DigitalEyewearBehaviour.Instance.SetViewerActive(true, true);
        } else
        {
            DigitalEyewearBehaviour.Instance.SetEyewearType(DigitalEyewearAbstractBehaviour.EyewearType.None);
            DigitalEyewearBehaviour.Instance.SetViewerActive(false, true);
        }
        /*
        CameraDevice.Instance.Stop();
        CameraDevice.Instance.Deinit();
        CameraDevice.Instance.Init(CameraDevice.CameraDirection.CAMERA_BACK);
        CameraDevice.Instance.SelectVideoMode(CameraDevice.CameraDeviceMode.MODE_DEFAULT);
        CameraDevice.Instance.Start();
        */
    }
}
