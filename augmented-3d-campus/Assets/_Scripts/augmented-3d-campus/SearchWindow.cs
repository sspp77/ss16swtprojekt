﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class SearchWindow : MonoBehaviour {


	public Button CloseButton;

	public Button SearchButton;
	public InputField SearchField;
	public GameObject ContentCanvas;
	public RectTransform RectTransform;

	public float AnimationSpeed;

	private bool hidden;
	public bool Hidden {
		get {
			return hidden;
		}
		set {
			if (value) {
			} else {
				Search ();
				Global.HidePanels ();
			}
			hidden = value;
		}	
	}

	public void Hide() {
		Hidden = true;
	}

	public void Show() {
		Global.GetSidebar ().Hide ();
		Global.GetTouchVisualizer ().Hidden = true;
		Hidden = false;
	}

	public void Search() {
		string[] searchTokens = SearchField.text.Split (' ');

		var buildings = Global.GetCampusData ().Buildings;
		var buildingKeys = buildings.Keys;

		var results = new List<ArrayList> ();

		foreach (var key in buildingKeys) {
			var matchList = getMatchListFromBuilding (key);
			foreach (String matcher in matchList) {
				foreach (var token in searchTokens) {
					if ((token.Length > 0 && matcher.ToLower ().Contains (token.ToLower ())) || SearchField.text.Length == 0) {
						results.Add (new ArrayList (){ key, matcher, matchList.IndexOf (matcher) });
					}
				}
			}
		}

		Debug.Log ("Search Result: " + results.Count);

		distinct (results);
		results.Sort(new MatchComparer());

		fillTable (results);
	}

	class MatchComparer : IComparer<ArrayList>
	{
		int IComparer<ArrayList>.Compare(ArrayList a, ArrayList b)
		{
			if (! ((Int32)a [2]).Equals((Int32)b [2])) {
				return ((Int32)a [2]).CompareTo ((Int32)b [2]);
			} else {
				return ((String)a [0]).CompareTo ((String)b [0]);
			}
		}
	}

	private List<String> getMatchListFromBuilding(string buildingID) {
		var list = new List<String> ();

		/* id */
		list.Add (buildingID);

		/* faculties */
		var building = Global.GetCampusData ().Buildings [buildingID];
		foreach (var facultyID in building.Faculties) {
			list.Add (facultyID);
			var faculty = Global.GetCampusData ().Faculties [facultyID];
			list.Add (faculty.Long);
			list.Add (faculty.Short);
		}

		/* contents */
		foreach (var content in building.Contents) {
			list.Add (content);
		}
		return list;
	}

	private void distinct(List<ArrayList> list) {
		var keys = new ArrayList ();

		for(int i = 0; i < list.Count; i++) {
			if (keys.Contains (list [i][0])) {
				list.RemoveAt (i--);
			} else {
				keys.Add(list[i][0]);
			}
		}
	}

	private void fillTable(List<ArrayList> results) {
		/* reset view  */
		foreach (Transform child in ContentCanvas.transform) {
			GameObject.Destroy(child.gameObject);
		}

		/* fill view */
		foreach (var match in results) {
			GameObject row = (GameObject)Instantiate(Resources.Load("augmented-3d-campus/Result Row"));
			row.transform.SetParent(ContentCanvas.transform);
			row.GetComponent<ResultRow> ().Fill (match);
			row.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
		}
	}


	void Start () {
		/* reset view  */
		foreach (Transform child in ContentCanvas.transform) {
			GameObject.Destroy(child.gameObject);
		}
		hidden = true;
		RectTransform.anchoredPosition = new Vector2 (0, -RectTransform.rect.height);

		RectTransform.sizeDelta = new Vector2 (Screen.width / transform.parent.localScale.x, Screen.height / transform.parent.localScale.y);
	}

	// Update is called once per frame
	void Update () {
		if (Hidden && RectTransform.anchoredPosition.y > 0) {
			RectTransform.anchoredPosition = new Vector2 (0, RectTransform.anchoredPosition.y - AnimationSpeed * Time.deltaTime);
		}
		if (Hidden && RectTransform.anchoredPosition.y < 0) {
			RectTransform.anchoredPosition = new Vector2 (0, 0);
		}

		if (!Hidden && RectTransform.anchoredPosition.y < RectTransform.rect.height) {
			RectTransform.anchoredPosition = new Vector2 (0, RectTransform.anchoredPosition.y + AnimationSpeed * Time.deltaTime);
		}
		if (!Hidden && RectTransform.anchoredPosition.y > RectTransform.rect.height) {
			RectTransform.anchoredPosition = new Vector2 (0, RectTransform.rect.height);
		}
	}
}
