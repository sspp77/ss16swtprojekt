﻿using UnityEngine;
using System.Collections;

public class InfoDialog : MonoBehaviour {

	void Start() {
		Hidden = true;
	}

	private bool hidden;
	/// <summary>
	/// Gets or sets a value indicating whether <see cref="QuitDialog"/> is hidden.
	/// </summary>
	/// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
	public bool Hidden {
		get {
			return hidden;
		}
		set {
			hidden = value;
			this.gameObject.GetComponent<Canvas> ().enabled = !hidden;
		}
	}

	/// <summary>
	/// Hides this <see cref="InfoDialog"/>
	/// </summary>
	public void Cancel() {
		Hidden = true;
	}
}
