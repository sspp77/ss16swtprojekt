﻿using System.Collections.Generic;
using System;
using UnityEngine;
using SimpleJSON;

/// <summary>
/// Represents all available campus data.
/// </summary>
public class CampusData: MonoBehaviour
{
	private MetaData meta;
	private Dictionary<String, FacultyData> faculties;
	private Dictionary<String, BuildingData> buildings;

	/// <summary>
	/// The JSON data file.
	/// </summary>
	public TextAsset data;

	/// <summary>
	/// The JSON schema file.
	/// </summary>
	public TextAsset schema;

	void Start () {
		JSONNode N = JSON.Parse(data.text);
		meta = new MetaData(N["meta"]);

		this.faculties = new Dictionary<String, FacultyData>();
		foreach (JSONNode faculty in N["faculties"].AsArray)
		{
			faculties.Add(faculty["number"], new FacultyData(faculty));
		}

		this.buildings = new Dictionary<String, BuildingData>();
		foreach (JSONNode building in N["buildings"].AsArray)
		{
			buildings.Add(building["name"], new BuildingData(building));
		}
	}

	/// <summary>
	/// Gets the meta data.
	/// </summary>
	/// <value>The meta data.</value>
	public MetaData Meta
	{
		get { return meta; }
	}

	/// <summary>
	/// Gets the faculties.
	/// </summary>
	/// <value>The faculties.</value>
	public Dictionary<String, FacultyData> Faculties
	{
		get { return faculties; }
	}

	/// <summary>
	/// Gets the buildings.
	/// </summary>
	/// <value>The buildings.</value>
	public Dictionary<String, BuildingData> Buildings 
	{
		get { return buildings; }
	}
}