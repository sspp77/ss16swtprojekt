﻿using System;
using System.Collections.Generic;
using SimpleJSON;

/// <summary>
/// Represents a faculty with data.
/// </summary>
public class FacultyData
{
	private String shrt;
	private String lng;

	/// <summary>
	/// Gets the short faculty identifier: "BAU", "EI", ...
	/// </summary>
	/// <value>The short faculty identifier.</value>
	public String Short
	{
		get { return shrt; }
	}

	/// <summary>
	/// Gets the long faculty identifier: "Bauwesen", "Gesundheit", ...
	/// </summary>
	/// <value>The long faculty identifier.</value>
	public String Long
	{
		get { return lng; }
	}

	/// <summary>
	/// Initializes a new instance of <see cref="FacultyData"/> using a JSON node.
	/// </summary>
	/// <param name="N">The JSON node used to build the faculty data object.</param>
	public FacultyData(JSONNode N)
	{
		shrt = N["short"].Value;
		lng = N["long"].Value;
	}
}