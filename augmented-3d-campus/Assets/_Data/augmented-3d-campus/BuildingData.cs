﻿using System;
using System.Collections.Generic;
using SimpleJSON;

/// <summary>
/// Represents a building with data.
/// </summary>
public class BuildingData
{
	private HashSet<String> contents;
	private HashSet<String> faculties;
	private String latitude;
	private String longitude;

	/// <summary>
	/// Gets the contents: "Mensa / Pastaria", "Vorlesungsräume", ...
	/// </summary>
	/// <value>The contents.</value>
	public HashSet<String> Contents
	{
		get { return contents; }
	}

	/// <summary>
	/// Gets the faculties: "01", "03", ...
	/// </summary>
	/// <value>The faculties.</value>
	public HashSet<String> Faculties
	{
		get { return faculties; }
	}

	/// <summary>
	/// Gets the longitude of the building (e.q. 8°41'06.4")
	/// </summary>
	/// <value>The longitude.</value>
	public String Longitude
	{
		get { return longitude; }
	}

	/// <summary>
	/// Gets the latitude of the building (e.q. 50°35'09.9")
	/// </summary>
	/// <value>The latitude.</value>
	public String Latitude
	{
		get { return latitude; }
	}

	/// <summary>
	/// Initializes a new instance of <see cref="BuildingData"/> using a JSON node.
	/// </summary>
	/// <param name="N">The JSON node used to build the building data object.</param>
	public BuildingData(JSONNode N)
	{
		contents = new HashSet<String>();
		foreach(object s in N["contents"].AsArray)
		{
			string content = s.ToString ();

			content = content.TrimEnd ('"').TrimStart ('"');

			contents.Add(content);
		}

		faculties = new HashSet<String>();
		foreach (object s in N["faculties"].AsArray)
		{
			string facutly = s.ToString ();

			facutly = facutly.TrimEnd ('"').TrimStart ('"');

			faculties.Add(facutly);
		}

		longitude = N ["longitude"];
		latitude = N ["latitude"];
	}

	/// <summary>
	/// Builds a string representing the building for debug purpose.
	/// </summary>
	public override string ToString() {
		return "Building -----\n" +
			"contents count: " + Contents.Count + "\n" +
			"facultiy count: " + Faculties.Count + "\n" + 
		"contents:\n" +
		Contents.ToString () + "\n" +
		"faculties:\n" +
		Faculties.ToString () + "\n" +
		"longitude " + Longitude.ToString () + "\n" +
		"latitude " + Latitude.ToString () + "\n" +
		"--------";
	}
}