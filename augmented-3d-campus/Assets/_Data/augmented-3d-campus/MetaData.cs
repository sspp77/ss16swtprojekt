﻿using System;
using System.Collections.Generic;
using SimpleJSON;

/// <summary>
/// Represents other data like format version.
/// </summary>
public class MetaData {
	private String version;

	/// <summary>
	/// Gets the JSON format version.
	/// </summary>
	/// <value>The version.</value>
	public String Version
	{
		get { return version; }
	}

	/// <summary>
	/// Initializes a new instance of <see cref="MetaData"/> using a JSON node.
	/// </summary>
	/// <param name="N">The JSON node.</param>
	public MetaData(JSONNode N)
	{
		version = N["format_version"].Value;
	}
}