{{InfoboxWMProjekt
|titel = SWT-P SS 2016 Augmented Campus 3D
|betreuer = Dennis Priefer, Wolf Rost
|mitarbeiter = Dominik Fahlenberg ([[Benutzer:dfhl60|dfhl60]]), Florian Fenzl ([[Benutzer:ffnz67|ffnz67]]), Samuel Schepp ([[Benutzer: sspp77|sspp77]]), Max Welker ([[Benutzer:mwlk04|mwlk04]]), David Zaremba ([[Benutzer:dzrm63|dzrm63]])
|start = 04/2016
|lang = C#
|framework = Vuforia, Unity
}}

= Beschreibung =
Im Rahmen des Bachelor Informatik Moduls "Softwaretechnik Projekt" entwickeln wir eine App für Smartphone und Tablet. Diese zeigt einem Studenten, der neu an der THM Gießen ist, den Campus. Die Projektion erfolgt durch Augmented Reality, indem ein realer Campusplan visuell durch 3D Gebäude ergänzt wird, indem der Nutzer den Plan mit unserer App abfilmt. Zur Realisierung setzen wir die Entwicklungsumgebung und Grafik-Engine "Unity" und das Augmented Reality Framework "Vuforia" ein. Zusätzlich zu den 3D Gebäuden sieht der Nutzer auch hilfreiche Informationen wie Gebäudenummern, Fachbereiche und wichtige Anlaufstellen. Dieser Campusplan ist ein großes Bild, welcher auf einem Tisch angebracht ist, welcher im Informationsgebäude des Campus Gießen steht. Der Campusplan wurde vorher mit kleinen Grafiken prepariert, sodass ein Computer erkennen kann, in welchem Winkel und an welcher Position der Plan abgefilmt wird.

= Git =
Unser Gitrepository https://git.thm.de/sspp77/ss16swtprojekt besteht aus drei Branches:
* master (Enthält das Haupt-Unityprojekt)
* documents (Enthält Dokumente und Präsentationsfolien usw)
* prototypes (Enthält Unity Projekte oder Code-Schnippsel, die wir zum Testen nutzen oder uns für später aufbewahren wollen)

= Rollen =
== Leitung ==

{| class="wikitable" style="text-align:left"
| '''Product Owner'''
| Dennis Priefer, Wolf Rost
|-
| '''Scrum-Master'''
| Samuel Schepp 
|-
|}

== Team ==
{| class="wikitable" style="text-align:left"
| '''Samuel Schepp'''
| Back-End, PHP 
|-
| '''Florian Fenzl''' 
| C#, Unity 
|-
| '''Dominik Fahlenberg''' 
| C#, Unity 
|-
| '''David Zaremba''' 
| C#, Unity 
|-
| '''Max Welker''' 
| 3D Modellierung, C#, Unity 
|-
|}

= Projektverlauf =

== Vor dem ersten Sprint ==
[[File:Grafik1.png|thumb|Die erste Idee: Der Campusplan auf einem Tisch.]]
Beim ersten Treffen am 12. April 2016 wurden uns durch den Product Owner Dennis Priefer verschiedene bereits existierende Projekte und Umsetzungen vorgestellt. Nach dem ersten Teammeeting entschieden wir uns für das oben beschriebene Projekt.
Uns ist Geschwindigkeit und einfache Bedienung sehr wichtig. Somit möchten wir auf GPS verzichten. Vor allem in geschlossenen Räumen ist die Ortung immer noch nicht schnell genug um in weniger als einer Sekunde eine akzeptable Eingrenzung des Aufenthaltsortes zu erreichen.
Im zweiten Meeting am 19. April besprachen wir die Stories für unseren ersten Sprint, welche am 20. April durch den Scum-Master unter Aufsicht der Product-Owner in das Jira Scrumboard eingepflegt wurden.

== Sprint 1 (19. April bis 3. Mai) ==

=== SWTPAR-334: Einarbeitung in Vuforia/Unity ===

[[Datei:Screenshot 2016-04-23 20.13.07.png|300px|thumb|Kleines Mockup: 2D Beschriftungen liegen im 3D Raum]]

Da sich erst einmal jeder für sich in Unity und Vuforia einarbeiten muss, soll sich jedes Gruppenmitglied eine Unteraufgabe erstellen.
Die Einarbeitung umfasst Tutorials, Tests, und das Entwerfen erster Ideen und Mockups in Unity.

Die Idee, welche im zweiten Gruppenmeeting besprochen wurde, Beschriftungen im 3D Raum als schwebende 2D Elemente darzustellen wurde im ersten Anlauf angetestet.

=== SWTPAR-336: Sichtung und erster Entwurf des Grundrisses ===

[[Datei:Thm-campus 3d.png|300px|thumb|Erster Entwurf des 3D-Modells]]

Das 3D-Modell soll nach ersten Überlegungen alle THM-Gebäude an der Wiesenstraße, dh. alle A-Gebäude beinhalten. Eventuell werden noch weiter Modelle folgen. Die Gebäude werden in maßstabsgetreuer Größe dargestellt, vergleichbar mit den 3D-Gebäuden aus Google Earth. Der Benutzer soll in der Lage sein das Modell zu drehen, sodass er jedes Gebäude uneingeschränkt betrachten kann falls sich Gebäude gegenseitig überdecken. Außerdem soll es möglich sein, ein einzelnes Gebäude auszuwählen, wodurch alle anderen ausgeblendet werden.

=== SWTPAR-337: Überlegungen zum Gestell (Dimensionen etc.) ===

Als Form würde sich ein viereckiger Tisch der frei im Raum steht am ehesten anbieten. So kann das 3D-Modell von allen Seiten betrachtet werden.

Eine Stoffdecke oder eine möglichst schwach reflektierende Pappe mit bedruckten Muster wäre ideal, da so das Muster am besten erkannt werden kann und nicht durch Reflektionen gestört wird.

Das Muster ließe sich an verschiedensten Orten unterbringen, da eine Unterlage mit Muster auf einem Tisch die Arbeitsfläche nicht einschränkt. So wäre auch theoretisch eine Darstellung auf Mensatischen oder in der Bücherei möglich.

Der Test mit einem 40x40cm großen Image Target verlief erfolgreich. Das Muster lässt sich auch bei naher Zoomstufe erkennen und von überall betrachten.

Boden wäre nur bei einem sehr großen Muster zu empfehlen, da für Details bei kleineren Mustern immer in die Hocke gegangen werden müsste.
Für ein 40x40cm großes Muster würde sich ein hüfthoher Tisch anbieten.

=== SWTPAR-338: Überlegungen zu benötigten Daten ===

Die App basiert sowohl auf den Daten der vorhandenen REST-Schnitstellen
* http://www-test.mni.thm.de/ar/rest/index.php/
und
* http://www-test.mni.thm.de/mni/rest/index.php

Die Schnittstellen erlauben Zugriff auf
* Dozent
* Stundenpläne
* Fächer

Aufgrund fehlender Informationen zu den jeweiligen REST-Parametern ("key", "scheduleid", "date") war es noch nicht möglich die Schnittstelle zu testen. Wir möchten den Product-Owner und den Betreiber der Schnittstelle um weitere Informationen bitten.

Außerdem möchten wir einen eigenen Webservice aufsetzen, der Informationen zu Spezialgebäuden/-räumen liefert:
* Info
* Fachschaftsräume
* Fachbereiche
* ASTA
* Studien-/Fachschaftssekretariat, 
* Facility Management
* Bibliothek
* Mensen
* Sportstätten, uvm

Auch der Speiseplan soll durch HTML-Parsing angezeigt werden.

=== SWTPAR-339: Vorhandene 3D Modelle prüfen ===

Die Modelle aus AuCaNa konnten wir uns noch nicht anschauen, da wir noch keinen Zugang zum Git hatten. Daher wurde diese Story in den nächsten Sprint übertragen.

== Sprint 2 (3. Mai bis 17. Mai) ==

=== SWTPAR-392: ImageTarget-Prüfung (große Targets) ===

=== SWTPAR-393: ImageTarget-Prüfung (kleine Targets) ===

=== SWTPAR-394: Schnittstelle prüfen / Eigener Service ===

=== SWTPAR-395: AuCaNa sichten, Maßstäbe prüfen ===

=== SWTPAR-399: Campus weiter designen ===
