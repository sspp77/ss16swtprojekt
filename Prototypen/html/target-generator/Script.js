var ctx;
var width;
var height;


function NakresliMarker() {
    // get the canvas element using the DOM
  var canvas = document.getElementById('marker');
 
  // Make sure we don't execute when canvas isn't supported
  if (canvas.getContext){

      ZobrazVarovanie();

    // use getContext to use the canvas for drawing
    ctx = canvas.getContext('2d');

    width = canvas.width;
    height = canvas.height;


    ctx.fillStyle   = '#fff'; // white
    ctx.fillRect  (0,   0, width, height);

    //Ciary
    if (document.getElementById("ciary").checked) 
    {
        var Krok = 120 - document.getElementById("ciarymnozstvo").selectedIndex * 20;
        for (var x = 1; x < width;x+=Krok )
        {
            for (var y = 1; y < height;y+=Krok )
            {
                var x1=x+Math.random()*10;
                var y1=y+Math.random()*10;
                var x2=x+Math.random()*width/2*Plus();
                var y2=y+Math.random()*height/2*Plus();
                var x3=x+Math.random()*width/2*Plus();
                var y3=y+Math.random()*height/2*Plus();

                var hrubka=2+Math.random()*8;

                NakresliCiaru(x1,y1,x2,y2,hrubka);            
            }
        }
    }

    //Trojuholniky
    if (document.getElementById("trojuholniky").checked) 
    {
        var Krok = 140 - document.getElementById("trojuholnikymnozstvo").selectedIndex * 40;
        for (var x = 1; x < width; x += Krok) {
            for (var y = 1; y < height; y += Krok) {
                var x1 = x + Math.random() * 10;
                var y1 = y + Math.random() * 10;
                var x2 = x + Math.random() * width / 4 * Plus();
                var y2 = y + Math.random() * height / 4 * Plus();
                var x3 = x + Math.random() * width / 4 * Plus();
                var y3 = y + Math.random() * height / 4 * Plus();

                var hrubka = 5;

                NakresliTrojuholnik(x1, y1, x2, y2, x3, y3, hrubka)
            }
        }
    }

    //Stvoruholniky
    if (document.getElementById("stvoruholniky").checked) 
    {
        var Krok = 140 - document.getElementById("stvoruholnikymnozstvo").selectedIndex * 40;
        for (var x = 1; x < width; x += Krok) {
            for (var y = 1; y < height; y += Krok) {
                var x1 = x + Math.random() * 10;
                var y1 = y + Math.random() * 10;
                var x2 = x + Math.random() * width / 5;
                var y2 = y + Math.random() * height / 5 * Plus();
                var x3 = x + Math.random() * width / 5;
                var y3 = y2 + Math.random() * height / 5;
                var x4 = x3 - Math.random() *  height / 5;
                var y4 = y + 10 + Math.random() * height / 5;

                var hrubka = 5;

                NakresliStvoruholnik(x1, y1, x2, y2, x3, y3, x4, y4, hrubka)
            }
        }
    }

      // save canvas image as data url (png format by default) var dataURL = canvas.toDataURL('image/jpeg');
      var dataURL = canvas.toDataURL();

      // set canvasImg image src to dataURL
      // so it can be saved as an image
      document.getElementById('markerImg').src = dataURL;
  }
}

function Plus() {
    var p = 1;
    if (Math.random() < 0.5) 
    {
        p = -1;
    }
    return p;
}

function NakresliCiaru(x1,y1,x2,y2,hrubka) {
    ctx.beginPath();
    ctx.moveTo(x1,y1);
    ctx.lineTo(x2,y2);
    ctx.closePath();
    ctx.lineWidth = hrubka;
    ctx.stroke();
}

function NakresliTrojuholnik(x1,y1,x2,y2,x3,y3,hrubka) {
    ctx.beginPath();
    ctx.moveTo(x1,y1);
    ctx.lineTo(x2,y2);
    ctx.lineTo(x3,y3);
    ctx.closePath();
    ctx.fillStyle = '#dfdfdf';
    if (document.getElementById("farba").checked) 
    {
        ctx.fillStyle = get_random_color();
    }
    ctx.fill();
    ctx.lineWidth = hrubka;
    ctx.stroke();

}

function NakresliStvoruholnik(x1,y1,x2,y2,x3,y3,x4,y4,hrubka) {
    ctx.beginPath();
    ctx.moveTo(x1,y1);
    ctx.lineTo(x2,y2);
    ctx.lineTo(x3,y3);
    ctx.lineTo(x4,y4);
    ctx.closePath();
    ctx.fillStyle = '#dfdfdf';
    if (document.getElementById("farba").checked) 
    {
        ctx.fillStyle = get_random_color();
    }
    ctx.fill();
    ctx.lineWidth = hrubka;
    ctx.stroke();

}

function ZobrazVarovanie() {
    var Varovanie = "";
    var Pocet=0;
    if (document.getElementById("ciary").checked)
    {
        Pocet += 1;
        if (document.getElementById("ciarymnozstvo").selectedIndex>0)
        {
            Pocet += 1;
        }
    }
    if (document.getElementById("trojuholniky").checked)
    {
        Pocet += 1;
        if (document.getElementById("trojuholnikymnozstvo").selectedIndex>0)
        {
            Pocet += 1;
        }
    }
    if (document.getElementById("stvoruholniky").checked)
    {
        Pocet += 1;
        if (document.getElementById("stvoruholnikymnozstvo").selectedIndex>0)
        {
            Pocet += 1;
        }
    }

    if (Pocet<2)
    {
        Varovanie = "Low tracking warning! Please combine more elements!";
    }
    document.getElementById("warning").innerHTML = Varovanie;
}

function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}