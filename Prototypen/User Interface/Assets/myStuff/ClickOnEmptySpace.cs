﻿using UnityEngine;
using System.Collections;

public class ClickOnEmptySpace : MonoBehaviour {

    GameObject[] blocks;

    // Use this for initialization
    void Start () {

        blocks = GameObject.FindGameObjectsWithTag("cube");

    }
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnMouseDown()
    {
        for (int i = 0; i < blocks.Length; i++)
        {
            foreach (Transform t in transform)
            {
                if (t.tag == "canvas") t.gameObject.GetComponent<Canvas>().enabled = false;
            }
            blocks[i].GetComponent<SelectionHandler>().selected = false;
        }

    }
}
