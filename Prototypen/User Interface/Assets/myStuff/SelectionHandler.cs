﻿using UnityEngine;
using System.Collections;

public class SelectionHandler : MonoBehaviour {

    public Color objectColor;
    GameObject[] blocks;
    public bool selected;
    

	// Use this for initialization
	void Start () {

        objectColor = gameObject.GetComponent<Renderer>().material.color;
        blocks = GameObject.FindGameObjectsWithTag("cube");
        selected = false;
        foreach (Transform t in transform)
        {
            if (t.tag == "canvas") t.gameObject.GetComponent<Canvas>().enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
        
        

    }

    void OnMouseDown() {

        reset();
        selected = !selected;
        foreach (Transform t in transform)
        {
            if (t.tag == "canvas") t.gameObject.GetComponent<Canvas>().enabled = true;
        }

    }

    

    public void reset() {
        for(int i = 0; i< blocks.Length; i++) {
            foreach (Transform t in blocks[i].transform)
            {
                if (t.tag == "canvas") t.gameObject.GetComponent<Canvas>().enabled = false;
            }

            blocks[i].GetComponent<SelectionHandler>().selected = false;
        }
    }
}
