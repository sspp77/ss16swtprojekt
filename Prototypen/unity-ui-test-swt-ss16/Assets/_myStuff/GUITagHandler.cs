﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class GUITagHandler : MonoBehaviour {

	public string Title;
	public string Text;
	public GameObject Parent;

	private MeshRenderer parentMesh;
	private GameObject arCamera;

	private Text titleObject;
	private Text textObject;

	private Canvas myCanvas;

	// Use this for initialization
	void Start () {
		// Collect Objects
		titleObject = transform.Find ("title").gameObject.GetComponent<Text>();
		textObject = transform.Find ("text").gameObject.GetComponent<Text>();
		parentMesh = Parent.GetComponent<MeshRenderer> ();
		myCanvas = this.gameObject.GetComponent<Canvas> ();
		arCamera = GameObject.Find ("ARCamera");
	}
	
	// Update is called once per frame
	void Update () {
		if (titleObject != null) {
			titleObject.text = Title;
		}
		if (textObject != null) {
			textObject.text = Text;
		}
		if (arCamera != null) {
			transform.rotation = arCamera.transform.rotation;
			transform.Rotate (0, 0, 0);
		}
		if (myCanvas != null) {
			myCanvas.enabled = parentMesh.enabled;
		}
	}
}
