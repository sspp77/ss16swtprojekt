﻿using UnityEngine;
using System.Collections;

public class SpawnStudents : MonoBehaviour {

    public float spawnIntervalMin = 1.0f;
    public float spawnIntervalMax = 3.0f;

    public GameObject studentPrefab;

    public StudentBase.WayPoint[] waypoints;

    public bool loop = true;
    public bool destroyWhenDone = false;

    public float radius = 2.0f;

    private float timePassed = 0.0f;
    private float nextSpawnInterval;
    

	// Use this for initialization
	void Start () {
        nextSpawnInterval = Random.Range(spawnIntervalMin, spawnIntervalMax);
	}
	
	// Update is called once per frame
	void Update () {
        if (timePassed >= nextSpawnInterval)
        {
            Vector2 randRad = Random.insideUnitCircle * radius;
            Vector3 pos = new Vector3(transform.position.x + randRad.x, transform.position.y, transform.position.z + randRad.y);
            GameObject cur = Instantiate(studentPrefab, pos, Quaternion.identity) as GameObject;

            StudentBase.WayPoint[] newWP = new StudentBase.WayPoint[waypoints.Length];
            for(int i = 0; i < waypoints.Length; i++)
            {
                newWP[i] = new StudentBase.WayPoint();
                newWP[i].location = waypoints[i].location;
                newWP[i].radius = waypoints[i].radius;
                newWP[i].waitDuration = waypoints[i].waitDuration;
            }

            StudentBase curBase = cur.GetComponent<StudentBase>();

            curBase.waypoints = newWP;
            curBase.loop = loop;
            curBase.destroyWhenDone = destroyWhenDone;
            nextSpawnInterval = Random.Range(spawnIntervalMin, spawnIntervalMax);
            timePassed = 0.0f;
        }
        else timePassed += Time.deltaTime;
        
	}
}
